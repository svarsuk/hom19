﻿#include <iostream>
using namespace std;


class Animal 
{
public:
    virtual void Sound() 
    {

        cout << "nosie";

    }

};

class Wolf : public Animal
{
public:
    void Sound() override
    {
        cout << " Wolf Auf " << endl;

    }





};

class Fox : public Animal
{
public:
    void Sound() override
    {
        cout << " Fox Aeeee" << endl;

    }

};
 
class octopuss : public Animal
{
public:
    void Sound() override
    {
        cout << " octopuss mrgl" << endl;

    }

};

int main()
{
    const int Size = 3;
    Animal* animals[Size] = { new Wolf, new Fox, new octopuss };

    for (Animal* a : animals)
        a -> Sound();
}

